from __future__ import annotations

import logging
import sys

from csa_lab3.isa import Instruction
from csa_lab3.machine.control_unit import ControlUnit


def parse_isa(lines: list[str]) -> list[Instruction]:
    res: list[Instruction] = []
    for line in lines:
        try:
            res.append(Instruction.parse(line.split(maxsplit=1)[1]))
        except ValueError:
            pass
        except IndexError:
            pass
    return res


def main(code_file: str, input_file: str):
    with open(code_file) as code:
        program = parse_isa(code.readlines())
    with open(input_file) as inp:
        input_data = [*(inp.readline())]
    control_unit = ControlUnit(input_data, program)
    try:
        while True:
            control_unit.tick()
    except StopIteration:
        pass
    print("".join(control_unit.datapath.output.output_buffer))


if __name__ == "__main__":
    if len(sys.argv) == 4:
        logging.basicConfig(filename=sys.argv[3], level=logging.DEBUG, format="%(message)s")
    main(sys.argv[1], sys.argv[2])
