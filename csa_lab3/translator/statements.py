from __future__ import annotations

from csa_lab3.isa import ArgType, Instruction, Opcode
from csa_lab3.translator.common import AstExprType, AstVariable, ParseError, TranslateError
from csa_lab3.translator.expressions import AstExpr, AstInput, ExpressionParseError
from csa_lab3.translator.tokenizer import Token, Tokenizer, TokenType
from csa_lab3.translator.translator import Mark, MarkedInstr, Translator, resolve_marks


def _set_lines(
    instrs: list[Instruction | Mark | MarkedInstr], line: int | None
) -> list[Instruction | Mark | MarkedInstr]:
    if line is None:
        return instrs
    for i in instrs:
        if isinstance(i, Instruction):
            if i.line is None:
                i.line = line
        if isinstance(i, MarkedInstr):
            if i.instr.line is None:
                i.instr.line = line
    return instrs


def translate(translator: Translator, stmts: list[AstStmt]) -> list[Instruction]:
    res: list[Instruction | Mark | MarkedInstr] = []
    for stmt in stmts:
        stmt_instrs = stmt.translate(translator)
        if stmt_instrs:
            _set_lines(stmt_instrs, stmt.line)
            res.extend(stmt_instrs)
        else:
            raise TranslateError(message="Statement translate error")
    res.append(Instruction(Opcode.HALT))
    return resolve_marks([*translator.preload, *res])


class AstStmt:
    def __init__(self) -> None:
        super().__init__()
        self.line: int | None = None

    @staticmethod
    def parse(tokenizer: Tokenizer, sym_table: dict[str, AstVariable]) -> AstStmt | None:
        token = tokenizer.get_next_token()
        if token is None:
            return None
        line = token.line
        tokenizer.return_token(token)

        for c in [AstInput, AstAssign, AstDecl, AstIf, AstWhile, AstOutput]:
            stmt = c.parse(tokenizer, sym_table)
            if stmt:
                stmt.line = line
                return stmt
        return None

    def translate(self, translator: Translator) -> list[Instruction | Mark | MarkedInstr]:
        raise NotImplementedError()


def _check_next_token(tokenizer: Tokenizer, expected: TokenType) -> Token:
    token = tokenizer.get_next_token()
    if not token or token.token_type != expected:
        raise ParseError(token, expected)
    return token


def _get_next_expr(tokenizer: Tokenizer, sym_table: dict[str, AstVariable]) -> AstExpr:
    try:
        expr = AstExpr.parse(tokenizer, sym_table)
        if not expr:
            raise ParseError(message="Failed to parse: expected expression")
    except ExpressionParseError as ex:
        raise ParseError(message="Failed to parse: expected expression") from ex
    return expr


def _parse_expr_and_stmts_in_braces(
    tokenizer: Tokenizer, sym_table: dict[str, AstVariable]
) -> tuple[AstExpr, list[AstStmt]]:
    _check_next_token(tokenizer, TokenType.L_PAR)

    expr = _get_next_expr(tokenizer, sym_table)
    if expr.type != AstExprType.INT:
        raise ParseError(message=f"Expected condition with result type INT, got {expr.type.name}")

    _check_next_token(tokenizer, TokenType.R_PAR)

    _check_next_token(tokenizer, TokenType.L_BRACE)

    stmt = AstStmt.parse(tokenizer, sym_table)
    stmts: list[AstStmt] = []
    while stmt:
        stmts.append(stmt)
        stmt = AstStmt.parse(tokenizer, sym_table)

    _check_next_token(tokenizer, TokenType.R_BRACE)

    return expr, stmts


class AstIf(AstStmt):
    def __init__(self, expr: AstExpr, statements: list[AstStmt]) -> None:
        super().__init__()
        self.expr = expr
        self.statements = statements

    @staticmethod
    def parse(tokenizer: Tokenizer, sym_table: dict[str, AstVariable]) -> AstIf | None:
        token = tokenizer.get_next_token()
        if not token or token.token_type != TokenType.IF:
            tokenizer.return_token(token)
            return None

        return AstIf(*_parse_expr_and_stmts_in_braces(tokenizer, sym_table))

    def translate(self, translator: Translator) -> list[Instruction | Mark | MarkedInstr]:
        res = self.expr.translate(translator)

        mark = Mark()

        res.append(MarkedInstr(Instruction(Opcode.JZ, -1, ArgType.IMMEDIATE), mark))
        for s in self.statements:
            res.extend(_set_lines(s.translate(translator), s.line))
        res.append(mark)

        return res

    def __str__(self) -> str:
        newline = "\n"
        return f"If statement: [expr: {self.expr}] at {self.line}\n[statements: {newline.join(str(i) for i in self.statements)}]"


class AstWhile(AstStmt):
    def __init__(self, expr: AstExpr, statements: list[AstStmt]) -> None:
        super().__init__()
        self.expr = expr
        self.statements = statements

    @staticmethod
    def parse(tokenizer: Tokenizer, sym_table: dict[str, AstVariable]) -> AstWhile | None:
        token = tokenizer.get_next_token()
        if not token or token.token_type != TokenType.WHILE:
            tokenizer.return_token(token)
            return None

        return AstWhile(*_parse_expr_and_stmts_in_braces(tokenizer, sym_table))

    def translate(self, translator: Translator) -> list[Instruction | Mark | MarkedInstr]:
        res: list[Instruction | Mark | MarkedInstr] = []

        mark_loop = Mark()
        mark_after = Mark()

        res.append(mark_loop)

        res.extend(self.expr.translate(translator))

        res.append(MarkedInstr(Instruction(Opcode.JZ, -1, ArgType.IMMEDIATE), mark_after))
        for s in self.statements:
            res.extend(_set_lines(s.translate(translator), s.line))
        res.append(MarkedInstr(Instruction(Opcode.JMP, -1, ArgType.IMMEDIATE), mark_loop))
        res.append(mark_after)

        return res

    def __str__(self) -> str:
        newline = "\n"
        return f"While statement: [expr: {self.expr}] at {self.line}\nstatements:\n{newline.join(str(i) for i in self.statements)}\nend_of_statements"


class AstOutput(AstStmt):
    def __init__(self, expr: AstExpr) -> None:
        super().__init__()
        self.expr = expr

    @staticmethod
    def parse(tokenizer: Tokenizer, sym_table: dict[str, AstVariable]) -> AstOutput | None:
        token = tokenizer.get_next_token()
        if not token or token.token_type != TokenType.PRINT:
            tokenizer.return_token(token)
            return None

        token = _check_next_token(tokenizer, TokenType.L_PAR)

        expr = _get_next_expr(tokenizer, sym_table)
        if expr.type != AstExprType.CHAR and expr.type != AstExprType.STRING:
            raise ParseError(message="Wrong type! Expected CHAR or STRING expression type as print() argument")

        token = _check_next_token(tokenizer, TokenType.R_PAR)

        _check_next_token(tokenizer, TokenType.SEMICOLON)

        return AstOutput(expr)

    def translate(self, translator: Translator) -> list[Instruction | Mark | MarkedInstr]:
        res = self.expr.translate(translator)
        if self.expr.type == AstExprType.CHAR:
            res.append(Instruction(Opcode.ST, translator.output_addr, ArgType.IMMEDIATE))
            return res

        read = translator.allocate_for_tmp_expr()
        addr = translator.allocate_for_tmp_expr()
        cnt = translator.allocate_for_tmp_expr()

        loop = Mark()

        res.append(Instruction(Opcode.ST, addr, ArgType.IMMEDIATE))
        res.append(Instruction(Opcode.LD, addr, ArgType.INDIRECT))

        res.append(loop)
        res.append(Instruction(Opcode.ST, cnt, ArgType.IMMEDIATE))

        res.append(Instruction(Opcode.LD, addr, ArgType.DIRECT))
        res.append(Instruction(Opcode.ADD, 1, ArgType.IMMEDIATE))
        res.append(Instruction(Opcode.ADD, addr, ArgType.INDIRECT))
        res.append(Instruction(Opcode.SUB, cnt, ArgType.DIRECT))

        res.append(Instruction(Opcode.ST, read, ArgType.IMMEDIATE))
        res.append(Instruction(Opcode.LD, read, ArgType.INDIRECT))
        res.append(Instruction(Opcode.ST, translator.output_addr, ArgType.IMMEDIATE))

        res.append(Instruction(Opcode.LD, cnt, ArgType.DIRECT))
        res.append(Instruction(Opcode.SUB, 1, ArgType.IMMEDIATE))
        res.append(MarkedInstr(Instruction(Opcode.JNZ, -1, ArgType.IMMEDIATE), loop))

        return res

    def __str__(self) -> str:
        return f"Output stmt: [expr {self.expr}] at {self.line}"


class AstDecl(AstStmt):
    def __init__(
        self, var_type: AstExprType, var_name: str, expr: AstExpr | None = None, str_len: int | None = None
    ) -> None:
        super().__init__()
        self.var_type = var_type
        self.str_len = str_len
        self.var_name = var_name
        self.expr = expr

    @staticmethod
    def parse(tokenizer: Tokenizer, sym_table: dict[str, AstVariable]) -> AstDecl | None:
        decl = AstDecl._parse_decl(tokenizer, sym_table)
        if not decl:
            return None
        if decl.var_name in sym_table:
            raise ParseError(message=f"Variable {decl.var_name} already exists")
        sym_table[decl.var_name] = AstVariable(decl.var_type, decl.var_name)
        return decl

    @staticmethod
    def _parse_decl(tokenizer: Tokenizer, sym_table: dict[str, AstVariable]) -> AstDecl | None:
        token = tokenizer.get_next_token()
        if not token:
            tokenizer.return_token(token)
            return None

        match token.token_type:
            case TokenType.INT:
                var_type = AstExprType.INT
            case TokenType.CHAR:
                var_type = AstExprType.CHAR
            case TokenType.STRING:
                var_type = AstExprType.STRING
            case _:
                tokenizer.return_token(token)
                return None

        token = _check_next_token(tokenizer, TokenType.VAR_LIT)

        var_name = token.value
        token = tokenizer.get_next_token()
        if not token or token.token_type != TokenType.ASSIGN:
            str_len = None
            if var_type == AstExprType.STRING and token and token.token_type == TokenType.L_BRACKET:
                token = _check_next_token(tokenizer, TokenType.NUM_LIT)
                str_len = int(token.value)
                token = _check_next_token(tokenizer, TokenType.R_BRACKET)
                token = tokenizer.get_next_token()
            if not token or token.token_type != TokenType.SEMICOLON:
                raise ParseError(token, TokenType.SEMICOLON)
            return AstDecl(var_type, var_name, str_len=str_len)

        expr = _get_next_expr(tokenizer, sym_table)
        if expr.type != var_type:
            raise ParseError(message=f"Expected expr with type {var_type.name}, got {expr.type.name}")

        _check_next_token(tokenizer, TokenType.SEMICOLON)

        return AstDecl(var_type, var_name, expr)

    def translate(self, translator: Translator) -> list[Instruction | Mark | MarkedInstr]:
        var_addr = translator.allocate_var(self.var_name)
        res: list[Instruction | Mark | MarkedInstr] = []
        if self.str_len is not None:
            res = [
                Instruction(Opcode.LD, translator.allocate_data(self.str_len + 1, [self.str_len]), ArgType.IMMEDIATE)
            ]
        elif self.expr is not None:
            res = self.expr.translate(translator)
        else:
            res = [Instruction(Opcode.LD, 0, ArgType.IMMEDIATE)]
        if len(res) == 0:
            return []
        res.append(Instruction(Opcode.ST, var_addr, ArgType.IMMEDIATE))
        return res

    def __str__(self) -> str:
        return (
            f"Decl statements: [var_type {self.var_type.name}{' ' + str(self.str_len) if self.str_len else ''}]"
            f"[var_name {self.var_name}] [expr {self.expr}] at {self.line}"
        )


class AstAssign(AstStmt):
    def __init__(self, dst: AstExpr, expr: AstExpr) -> None:
        super().__init__()
        self.dst = dst
        self.expr = expr

    @staticmethod
    def parse(tokenizer: Tokenizer, sym_table: dict[str, AstVariable]) -> AstAssign | None:
        dst_expr = AstExpr.parse(tokenizer, sym_table)
        if not dst_expr:
            return None

        if not dst_expr.assignable:
            raise ParseError(message="Unable to assign value to this expression")

        _check_next_token(tokenizer, TokenType.ASSIGN)

        expr = _get_next_expr(tokenizer, sym_table)

        if dst_expr.type != expr.type:
            raise ParseError(message=f"Unable to assign type {expr.type.name} to {dst_expr.type.name}")

        _check_next_token(tokenizer, TokenType.SEMICOLON)

        return AstAssign(dst_expr, expr)

    def translate(self, translator: Translator) -> list[Instruction | Mark | MarkedInstr]:
        dst_arg, res, to_free = self.dst.translate_to_address_arg(translator)

        res.extend(self.expr.translate(translator))

        res.append(Instruction(Opcode.ST, dst_arg[0], dst_arg[1]))

        if to_free:
            translator.free_tmp_expr(dst_arg[0])

        return res

    def __str__(self) -> str:
        return f"Assign statements: [dst {self.dst}] [expr {self.expr}] at {self.line}"
