from __future__ import annotations

import enum
from types import MappingProxyType

from csa_lab3.isa import ArgType, Instruction, Opcode
from csa_lab3.translator.common import AstExprType, AstVariable, ParseError
from csa_lab3.translator.tokenizer import Token, Tokenizer, TokenType
from csa_lab3.translator.translator import Mark, MarkedInstr, TranslateError, Translator


@enum.unique
class AstOpType(enum.Enum):
    # logical not
    NOT = 5, [([AstExprType.INT], AstExprType.INT)], enum.auto()

    UN_MINUS = 5, [([AstExprType.INT], AstExprType.INT)], enum.auto()

    MUL = 4, [([AstExprType.INT, AstExprType.INT], AstExprType.INT)], enum.auto()
    DIV = 4, [([AstExprType.INT, AstExprType.INT], AstExprType.INT)], enum.auto()
    MOD = 4, [([AstExprType.INT, AstExprType.INT], AstExprType.INT)], enum.auto()

    PLUS = (
        3,
        [
            ([AstExprType.INT, AstExprType.INT], AstExprType.INT),
            ([AstExprType.INT, AstExprType.CHAR], AstExprType.CHAR),
            ([AstExprType.CHAR, AstExprType.INT], AstExprType.CHAR),
        ],
        enum.auto(),
    )
    BIN_MINUS = (
        3,
        [
            ([AstExprType.INT, AstExprType.INT], AstExprType.INT),
            ([AstExprType.CHAR, AstExprType.CHAR], AstExprType.INT),
        ],
        enum.auto(),
    )

    NEQ = (
        2,
        [
            ([AstExprType.INT, AstExprType.INT], AstExprType.INT),
            ([AstExprType.CHAR, AstExprType.CHAR], AstExprType.INT),
        ],
        enum.auto(),
    )
    EQ = (
        2,
        [
            ([AstExprType.INT, AstExprType.INT], AstExprType.INT),
            ([AstExprType.CHAR, AstExprType.CHAR], AstExprType.INT),
        ],
        enum.auto(),
    )
    LT = 2, [([AstExprType.INT, AstExprType.INT], AstExprType.INT)], enum.auto()
    GT = 2, [([AstExprType.INT, AstExprType.INT], AstExprType.INT)], enum.auto()

    # bitwise logical operations
    OR = 1, [([AstExprType.INT, AstExprType.INT], AstExprType.INT)], enum.auto()
    AND = 1, [([AstExprType.INT, AstExprType.INT], AstExprType.INT)], enum.auto()

    BRACKETS = -1, [([AstExprType.STRING, AstExprType.INT], AstExprType.CHAR)], enum.auto()

    L_PAR = -1, [], enum.auto()  # Special value to parse

    def __init__(self, priority: int, signatures: list[tuple[list[AstExprType], AstExprType]], num: int) -> None:
        super().__init__()
        self.priority = priority
        self.signatures = signatures
        self.num = num


class ExpressionParseError(Exception):
    "Exception raised for errors during expression parsing"

    def __init__(self, token: Token | None = None, message: str | None = None) -> None:
        if message is not None:
            super().__init__(f"Failed to parse expression: {message}")
            return
        if token is None:
            super().__init__("Failed to parse expression")
            return
        super().__init__(f"Failed to parse on token {token.token_type} with value '{token.value}'")


class AstExpr:
    def __init__(self, expr_type: AstExprType, assignable: bool = False) -> None:
        super().__init__()
        self.type = expr_type
        self.assignable = assignable

    @staticmethod
    def parse(tokenizer: Tokenizer, sym_table: dict[str, AstVariable]) -> AstExpr | None:
        return ExprParser().parse(tokenizer, sym_table)

    def translate(self, translator: Translator) -> list[Instruction | Mark | MarkedInstr]:
        raise NotImplementedError()

    def translate_to_address(self, translator: Translator) -> list[Instruction | Mark | MarkedInstr]:
        addr = self.get_addr(translator)
        if addr is not None:
            return [Instruction(Opcode.LD, addr, ArgType.DIRECT)]
        raise TranslateError(message="Unable to translate_to_address")

    def get_addr(self, translator: Translator) -> int | None:
        return None

    def get_immediate(self, translator: Translator) -> int | None:
        return None

    # returns arg, list of instructions to generate it,
    # and flag that indicates is it necessary to free arg in translator after use
    def translate_to_value_arg(
        self, translator: Translator
    ) -> tuple[tuple[int, ArgType], list[Instruction | Mark | MarkedInstr], bool]:
        res: list[Instruction | Mark | MarkedInstr]
        imm = self.get_immediate(translator)
        if imm is not None:
            return ((imm, ArgType.IMMEDIATE), [], False)

        addr = self.get_addr(translator)
        if addr is not None:
            return ((addr, ArgType.DIRECT), [], False)

        addr = translator.allocate_for_tmp_expr()
        res = self.translate(translator)
        res.append(Instruction(Opcode.ST, addr, ArgType.IMMEDIATE))
        return ((addr, ArgType.DIRECT), res, True)

    def translate_to_address_arg(
        self, translator: Translator
    ) -> tuple[tuple[int, ArgType], list[Instruction | Mark | MarkedInstr], bool]:
        addr = self.get_addr(translator)
        if addr is not None:
            return ((addr, ArgType.IMMEDIATE), [], False)

        addr = translator.allocate_for_tmp_expr()
        res = self.translate_to_address(translator)
        res.append(Instruction(Opcode.ST, addr, ArgType.IMMEDIATE))
        return ((addr, ArgType.DIRECT), res, True)


class AstExprOperation(AstExpr):
    def __init__(self, op_type: AstOpType, args: list[AstExpr]) -> None:
        super().__init__(
            AstExprOperation._process_type(op_type, args), (op_type == AstOpType.BRACKETS and args[0].assignable)
        )
        self.op_type = op_type
        self.args = args

    def __str__(self) -> str:
        if self.op_type in [AstOpType.UN_MINUS, AstOpType.NOT]:
            return f"({self.op_type.name}) ({self.args[0]})"
        return f"({self.op_type.name}) ({self.args[0]}) ({self.args[1]})"

    @staticmethod
    def _process_type(op_type: AstOpType, args: list[AstExpr]) -> AstExprType:
        for arg_types, res_type in op_type.signatures:
            if len(arg_types) == len(args):
                suitable = True
                for arg, arg_type in zip(args, arg_types):
                    if arg.type != arg_type:
                        suitable = False
                        break
                if suitable:
                    return res_type
        raise ExpressionParseError(
            message=f"Expression type error: there is no suitable signature for {op_type.name}, "
            f"arguments are {[arg.type.name for arg in args]}"
        )

    def translate(self, translator: Translator) -> list[Instruction | Mark | MarkedInstr] | None:
        return ExprTranslator.translate_operation(self, translator)

    def translate_to_address(self, translator: Translator) -> list[Instruction | Mark | MarkedInstr]:
        if self.op_type == AstOpType.BRACKETS and self.args[0].assignable:
            arg, res, to_free = self.args[1].translate_to_value_arg(translator)
            res.extend(self.args[0].translate_to_address(translator))
            res.append(Instruction(Opcode.ADD, arg[0], arg[1]))
            res.append(Instruction(Opcode.ADD, 1, ArgType.IMMEDIATE))
            if to_free:
                translator.free_tmp_expr(arg[0])
            return res
        raise TranslateError(message="Unable to translate_to_address unassignable expression")


class AstInput(AstExpr):
    def __init__(self) -> None:
        super().__init__(AstExprType.CHAR, False)
        self.line: int | None = None

    @staticmethod
    def parse(tokenizer: Tokenizer, sym_table: dict[str, AstVariable]) -> AstInput | None:
        res = AstInput.parse_body(tokenizer, sym_table)
        if not res:
            return None

        token = tokenizer.get_next_token()
        if not token or token.token_type != TokenType.SEMICOLON:
            raise ParseError(token, TokenType.SEMICOLON)

        return res

    @staticmethod
    def parse_body(tokenizer: Tokenizer, sym_table: dict[str, AstVariable]) -> AstInput | None:
        token = tokenizer.get_next_token()
        if not token or token.token_type != TokenType.GETC:
            tokenizer.return_token(token)
            return None

        token = tokenizer.get_next_token()
        if not token or token.token_type != TokenType.L_PAR:
            raise ParseError(token, TokenType.L_PAR)

        token = tokenizer.get_next_token()
        if not token or token.token_type != TokenType.R_PAR:
            raise ParseError(token, TokenType.R_PAR)

        return AstInput()

    def translate(self, translator: Translator) -> list[Instruction | Mark | MarkedInstr]:
        return [Instruction(Opcode.LD, translator.input_addr, ArgType.DIRECT)]

    def get_addr(self, translator: Translator) -> int | None:
        return translator.input_addr

    def __str__(self) -> str:
        return f"[Input stmt] at {self.line}"


@enum.unique
class AstLitType(enum.Enum):
    STR = 0
    VAR = 1
    NUM = 2
    CHAR = 3


class AstLit(AstExpr):
    def __init__(self, lit_type: AstLitType, val: str, sym_table: dict[str, AstVariable] | None = None) -> None:
        self.lit_type = lit_type
        match lit_type:
            case AstLitType.NUM:
                self.val = val
                super().__init__(AstExprType.INT)
            case AstLitType.STR:
                self.val = val
                super().__init__(AstExprType.STRING)
            case AstLitType.CHAR:
                self.val = val
                super().__init__(AstExprType.CHAR)
            case AstLitType.VAR:
                assert sym_table is not None
                self.val = val
                try:
                    var = sym_table[val]
                    super().__init__(var.type, True)
                except KeyError as kerror:
                    raise ExpressionParseError(message=f'Unknown variable "{val}"') from kerror

    def translate(self, translator: Translator) -> list[Instruction | Mark | MarkedInstr]:
        match self.lit_type:
            case AstLitType.VAR:
                return [Instruction(Opcode.LD, translator.get_var_addr(self.val), ArgType.DIRECT)]
            case AstLitType.CHAR:
                return [Instruction(Opcode.LD, ord(self.val), ArgType.IMMEDIATE)]
            case AstLitType.STR:
                return [
                    Instruction(
                        Opcode.LD,
                        translator.allocate_data(len(self.val) + 1, self._serialize_string(self.val)),
                        ArgType.IMMEDIATE,
                    )
                ]
            case AstLitType.NUM:
                return [Instruction(Opcode.LD, int(self.val), ArgType.IMMEDIATE)]

    @staticmethod
    def _serialize_string(s: str) -> list[int]:
        return [len(s)] + [ord(c) for c in s]

    def get_addr(self, translator: Translator) -> int | None:
        match self.lit_type:
            case AstLitType.VAR:
                return translator.get_var_addr(self.val)
            case AstLitType.STR:
                return translator.allocate_data(len(self.val) + 1, self._serialize_string(self.val))
            case _:
                return None

    def get_immediate(self, translator: Translator) -> int | None:
        match self.lit_type:
            case AstLitType.NUM:
                return int(self.val)
            case AstLitType.CHAR:
                return ord(self.val)
            case _:
                return None

    def __str__(self) -> str:
        return f"({self.lit_type.name}:{self.type.name}:{self.val})"


class ExprTranslator:
    def __init__(self) -> None:
        pass

    op_to_instr = MappingProxyType(
        {
            AstOpType.BIN_MINUS: Opcode.SUB,
            AstOpType.PLUS: Opcode.ADD,
            AstOpType.MUL: Opcode.MUL,
            AstOpType.DIV: Opcode.DIV,
            AstOpType.MOD: Opcode.REM,
            AstOpType.AND: Opcode.AND,
            AstOpType.OR: Opcode.OR,
        }
    )

    @staticmethod
    def _translate_brackets(
        expr: AstExprOperation, arg: tuple[int, ArgType], translator: Translator
    ) -> list[Instruction]:
        str_addr = expr.args[0].get_addr(translator)

        if str_addr is None:
            raise TranslateError(message="Expected that lhs argument of [] will have addr")

        char_addr = translator.allocate_for_tmp_expr()

        res: list[Instruction] = []

        res = [
            Instruction(Opcode.LD, str_addr, arg_type=ArgType.DIRECT),
            Instruction(Opcode.ADD, arg[0], arg[1]),
            Instruction(Opcode.ADD, 1, arg_type=ArgType.IMMEDIATE),
            Instruction(Opcode.ST, char_addr, arg_type=ArgType.IMMEDIATE),
            Instruction(Opcode.LD, char_addr, arg_type=ArgType.INDIRECT),
        ]

        translator.free_tmp_expr(char_addr)
        return res

    @staticmethod
    def _translate_op(expr: AstExprOperation, arg: tuple[int, ArgType], translator: Translator) -> list[Instruction]:
        if expr.op_type in ExprTranslator.op_to_instr:
            return [Instruction(ExprTranslator.op_to_instr[expr.op_type], arg[0], arg[1])]
        match expr.op_type:
            case AstOpType.EQ | AstOpType.GT | AstOpType.LT | AstOpType.NEQ:
                res = [Instruction(Opcode.CMP, arg[0], arg[1])]
                match expr.op_type:
                    case AstOpType.NEQ:
                        res.append(Instruction(Opcode.SETE))
                        res.append(Instruction(Opcode.NOT))
                    case AstOpType.EQ:
                        res.append(Instruction(Opcode.SETE))
                    case AstOpType.LT:
                        res.append(Instruction(Opcode.SETL))
                    case AstOpType.GT:
                        res.append(Instruction(Opcode.SETG))
                return res
            case _:
                raise TranslateError(message=f"Unexpected operation type {expr.op_type}")

    @staticmethod
    def _translate_bin(expr: AstExprOperation, translator: Translator) -> list[Instruction | Mark | MarkedInstr]:
        res: list[Instruction | Mark | MarkedInstr] = []

        arg, instrs, to_free = expr.args[1].translate_to_value_arg(translator)
        res.extend(instrs)

        if expr.op_type == AstOpType.BRACKETS:
            res.extend(ExprTranslator._translate_brackets(expr, arg, translator))
        else:
            res.extend(expr.args[0].translate(translator))
            res.extend(ExprTranslator._translate_op(expr, arg, translator))

        if to_free:
            translator.free_tmp_expr(arg[0])

        return res

    @staticmethod
    def translate_operation(expr: AstExprOperation, translator: Translator) -> list[Instruction | Mark | MarkedInstr]:
        if len(expr.args) == 1:
            res = expr.args[0].translate(translator)
            opcode = Opcode.NEG if expr.op_type == AstOpType.UN_MINUS else Opcode.NOT
            return [*res, Instruction(opcode)]
        return ExprTranslator._translate_bin(expr, translator)


class ExprParser:
    op_to_ast = MappingProxyType(
        {
            TokenType.PLUS: AstOpType.PLUS,
            TokenType.MINUS: AstOpType.BIN_MINUS,
            TokenType.MUL: AstOpType.MUL,
            TokenType.DIV: AstOpType.DIV,
            TokenType.MOD: AstOpType.MOD,
            TokenType.EQ: AstOpType.EQ,
            TokenType.LT: AstOpType.LT,
            TokenType.GT: AstOpType.GT,
            TokenType.OR: AstOpType.OR,
            TokenType.AND: AstOpType.AND,
            TokenType.L_BRACKET: AstOpType.BRACKETS,
            TokenType.NEQ: AstOpType.NEQ,
        }
    )

    @staticmethod
    def _convert_token_to_ast(token: Token, is_unary: bool) -> AstOpType:
        if is_unary:
            if token.token_type == TokenType.MINUS:
                return AstOpType.UN_MINUS
            if token.token_type == TokenType.NOT:
                return AstOpType.NOT
        return ExprParser.op_to_ast[token.token_type]

    def __init__(self) -> None:
        self.token_stack: list[AstOpType] = []
        self.res_stack: list[AstExpr] = []
        self.is_unary = True
        self.balance = 0

    def place_to_res(self, op: AstOpType) -> bool:
        if op == AstOpType.UN_MINUS or op == AstOpType.NOT:
            arg = self.res_stack.pop()
            self.res_stack.append(AstExprOperation(op, [arg]))
            return True
        arg2 = self.res_stack.pop()
        arg1 = self.res_stack.pop()
        self.res_stack.append(AstExprOperation(op, [arg1, arg2]))
        return True

    def process_op(self, token: Token) -> bool:
        op = ExprParser._convert_token_to_ast(token, self.is_unary)

        while self.token_stack:
            top: AstOpType = self.token_stack.pop()
            if top.priority < op.priority:
                self.token_stack.append(top)
                break
            if not self.place_to_res(top):
                return False
        self.token_stack.append(op)
        return True

    def process_lit(self, token: Token, sym_table: dict[str, AstVariable]) -> bool:
        match token.token_type:
            case TokenType.NUM_LIT:
                self.res_stack.append(AstLit(AstLitType.NUM, token.value))
            case TokenType.STR_LIT:
                self.res_stack.append(AstLit(AstLitType.STR, token.value))
            case TokenType.CHAR_LIT:
                self.res_stack.append(AstLit(AstLitType.CHAR, token.value))
            case TokenType.VAR_LIT:
                self.res_stack.append(AstLit(AstLitType.VAR, token.value, sym_table))
            case _:
                return False
        return True

    def flush_until(self, ast_type: AstOpType) -> bool:
        found = False
        while self.token_stack:
            top = self.token_stack.pop()
            if top == ast_type:
                found = True
                break
            if not self.place_to_res(top):
                return False
        return found

    def process_par(self, token: Token) -> bool:
        if token.token_type == TokenType.L_PAR:
            self.token_stack.append(AstOpType.L_PAR)
            return True

        return self.flush_until(AstOpType.L_PAR)

    def process_bracket(self, token: Token) -> bool:
        if token.token_type == TokenType.L_BRACKET:
            self.token_stack.append(AstOpType.BRACKETS)
            return True

        if not self.flush_until(AstOpType.BRACKETS):
            return False

        return self.place_to_res(AstOpType.BRACKETS)

    def process_getc(
        self,
        token: Token,
        sym_table: dict[str, AstVariable],
        tokenizer: Tokenizer,
    ) -> bool:
        tokenizer.return_token(token)
        inp = AstInput.parse_body(tokenizer, sym_table)
        if not inp:
            return False
        self.res_stack.append(inp)
        return True

    def flush_to_res(self) -> bool:
        while self.token_stack:
            top = self.token_stack.pop()
            if not self.place_to_res(top):
                return False
        return True

    def process_tokens(self, tokenizer: Tokenizer, sym_table: dict[str, AstVariable]):
        while True:
            process_res = True
            token = tokenizer.get_next_token()
            if not token:
                break

            match token.token_type:
                case (
                    TokenType.PLUS
                    | TokenType.MINUS
                    | TokenType.MUL
                    | TokenType.DIV
                    | TokenType.MOD
                    | TokenType.NOT
                    | TokenType.EQ
                    | TokenType.LT
                    | TokenType.GT
                    | TokenType.OR
                    | TokenType.AND
                    | TokenType.NEQ
                ):
                    process_res = self.process_op(token)
                    self.is_unary = True
                case TokenType.NUM_LIT | TokenType.STR_LIT | TokenType.VAR_LIT | TokenType.CHAR_LIT:
                    process_res = self.process_lit(token, sym_table)
                    self.is_unary = False
                case TokenType.R_PAR:
                    if self.balance == 0:
                        tokenizer.return_token(token)
                        break
                    self.balance -= 1
                    process_res = self.process_par(token)
                    self.is_unary = False
                case TokenType.L_PAR:
                    self.balance += 1
                    process_res = self.process_par(token)
                    self.is_unary = True
                case TokenType.R_BRACKET | TokenType.L_BRACKET:
                    process_res = self.process_bracket(token)
                    self.is_unary = token.token_type == TokenType.L_BRACKET
                case TokenType.GETC:
                    process_res = self.process_getc(token, sym_table, tokenizer)
                    self.is_unary = False
                case _:
                    tokenizer.return_token(token)
                    break

            if not process_res:
                raise ExpressionParseError(token)

    def parse(self, tokenizer: Tokenizer, sym_table: dict[str, AstVariable]) -> AstExpr | None:
        self.process_tokens(tokenizer, sym_table)
        self.flush_to_res()
        if len(self.res_stack) > 1:
            raise ExpressionParseError(message="res_stack wrong")
        if len(self.res_stack) == 0:
            return None

        return self.res_stack.pop()
