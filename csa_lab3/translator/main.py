import io
import sys
from itertools import groupby

from csa_lab3.translator.parser import Parser
from csa_lab3.translator.statements import translate
from csa_lab3.translator.translator import Translator


def main(src: str, dst: str, verbose: bool):
    with open(src) as source:
        parser = Parser(source)
        parser.parse()

        translator = Translator()
        res = translate(translator, parser.statements)

        with open(dst, mode="w+") as destination:
            if verbose:
                source.seek(0, io.SEEK_SET)
                lines = source.readlines()
                for line, instrs_in_line in groupby(enumerate(res), lambda x: x[1].line):
                    if line is None:
                        print("No line:", file=destination)
                    else:
                        print(f"{lines[line].strip()}:", file=destination)
                    for n, instr in instrs_in_line:
                        print(f"\t{n:<4} {instr}", file=destination)
            else:
                for n, instr in enumerate(res):
                    print(f"\t{n:<4} {instr}", file=destination)


if __name__ == "__main__":
    args = sys.argv
    src = args[1]
    dst = args[2]
    verbose = False

    if len(args) == 4:
        if args[3] == "-v":
            verbose = True
        else:
            raise ValueError()

    main(src, dst, verbose)
